package Code;

public class Program {
    public static void main(String args[])
    {
        String result;
        System.out.println("Welcome to the Movie Shop");
        Movie m1 = new Movie("m1", 1);
        Movie m2 = new Movie("m2", 2);
        Rental r1 = new Rental(m1, 7);
        Rental r2 = new Rental(m2, 3);

        Customer c1 = new Customer("jan");
        c1.addRental(r1);
        c1.addRental(r2);

        System.out.println("statement");
        result = c1.statement();
        System.out.println(result);
    }
}
