package Test;

import Code.Movie;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Code.Customer;
import Code.Rental;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CustomerTest {

    Customer customer;
    Rental rental1;
    Rental rental2;

    @Before
    public void setUp() throws Exception {
        customer = new Customer("Peter");
        rental1 = new Rental(new Movie("title1", 1), 7);
        rental2 = new Rental(new Movie("title2", 2), 3);
        customer.addRental(rental1);
        customer.addRental(rental2);
    }

    @After
    public void tearDown() throws Exception {
        customer = null;
    }

    @Test
    public void addRental() {
        Rental rental = new Rental(new Movie("test", 1), 2);
        customer.addRental(rental);
    }

    @Test
    public void getName() {
        assertEquals("Peter", customer.getName());
    }

     @Test
    public void statement()
     {
         assertTrue(customer.statement().startsWith("Rental Record for Peter"));
         assertTrue(customer.statement().contains("title1\t21.0"));
         assertTrue(customer.statement().contains("title2\t1.5"));
         assertTrue(customer.statement().endsWith(" frequent renter points"));
     }

}
